find7Best = (array) => {
    let best = Array(7).fill({
        id:-1,
        val:-1,
    });
    let d = array.length;
    console.log(array);
    if (d <= 7)
        return array;
    else {
        for(let i=0; i<d; i++) {
            let sub = array[i];
            for(let j=0; j<7; j++) {
                if (sub.val > best[j].val) {
                    let tmp = sub;
                    sub = best[j].val;
                    best[j].val = tmp;
                }
            }
        }

        return best;
    }
}

// tablica informująca, czy n-ty element jest wyświetlany 
let hide = null, colors;

// Rozwiązanie na podstawie https://github.com/chartjs/Chart.js/issues/2565
let weightChartLineOptions = {
    responsive: true,
    legendCallback: function(chart) {
        let newChart;
        console.log(chart);
        let legendHtml = [];
        legendHtml.push('<table id="spis">');
        legendHtml.push('<tbody style="display:flex;flex-direction:column;">');

        if (hide === null) {
            hide = new Array(0);
            newChart = true;
        } else {
            newChart = false;
        }

        for (let i=0; i<chart.data.datasets.length; i++) {
            legendHtml.push(`<tr style="order:2;" data-tr-id='${i}'>
            <td data-id='${i}'><input type="checkbox" data-ch-id='${i}' onclick="updateDataset(event, ${i}, ` + `\'` + chart.legend.legendItems[i].datasetIndex + `\'` + `)"></td>
            <td>${chart.data.datasets[i].label}</td>
            </tr>`);      

            if (newChart === true) {
                hide.push(true);
            }                                     
        }                                                                                  

        legendHtml.push('</tbody>');
        legendHtml.push('</table>');                                              
        return legendHtml.join("");                                                        
    },                                                                                     
    legend: {                                                                              
        display: false                                                                     
    }                                                                                      
};                

// Rozwiązanie z https://github.com/chartjs/Chart.js/issues/2565
let weightChartDoughnutOptions = {
    responsive: true,
    legendCallback: function(chart) {
        console.log(chart);
        let legendHtml = [];
        legendHtml.push('<table id="spis">');
        legendHtml.push('<tbody style="display:flex;flex-direction:column;">');

        for (let i=0; i<chart.data.datasets[0].data.length; i++) {
            legendHtml.push(`<tr style="order:2;" data-tr-id='${i}'>
            <td style="background-color:${chart.data.datasets[0].backgroundColor[i]};" data-id='${i}'></td>
            <td>${chart.data.labels[i]}</td>
            </tr>`);
                                                                          
        }                                                                                  
                              
        legendHtml.push('</tbody>');                                
        legendHtml.push('</table>');                                                       
        return legendHtml.join("");                                                        
    },                                                                                     
    legend: {                                                                              
        display: false                                                                     
    }                                                                                      
};    

// Show/hide chart by click legend
updateDataset = function(e, id, datasetIndex) {
    var index = datasetIndex;
    var ci = e.view.weightChart;
    var meta = ci.getDatasetMeta(index);

    // See controller.isDatasetVisible comment
    if (meta.hidden === null) {
        meta.hidden = !ci.data.datasets[index].hidden;
        hide[id] = false;
        document.querySelector(`tr[data-tr-id='${id}']`).style.order = 1;
        document.querySelector(`td[data-id='${id}']`).style.backgroundColor=`${ci.data.datasets[id].backgroundColor}`;
        document.querySelector(`input[data-ch-id='${id}']`).checked = true;
    } else {
        meta.hidden = null;
        hide[id] = true;
        document.querySelector(`tr[data-tr-id='${id}']`).style.order = 2;
        document.querySelector(`td[data-id='${id}']`).style.backgroundColor='';
        document.querySelector(`input[data-ch-id='${id}']`).checked = false;
    }

    // We hid a dataset ... rerender the chart
    ci.update();
};

const basicColorTable = [
    '#000000', '#FF0000', '#00FF00', '#0000FF', '#FFFF00',
    '#00FFFF', '#FF00FF', '#C0C0C0', '#808080', '#800000', '#808000',
    '#008000', '#800080', '#008080', '#000080'
]

const nbColorTable = [
    '#000000', '#00005f', '#000087', '#0000af', '#0000d7', '#0000ff',
    '#005f00', '#005f5f', '#005f87', '#005faf', '#005fd7', '#005fff',
    '#008700', '#00875f', '#008787', '#0087af', '#0087d7', '#0087ff',
    '#00af00', '#00af5f', '#00af87', '#00afaf', '#00afd7', '#00afff',
    '#00d700', '#00d75f', '#00d787', '#00d7af', '#00d7d7', '#00d7ff',
    '#00ff00', '#00ff5f', '#00ff87', '#00ffaf', '#00ffd7', '#00ffff',
    '#5f0000', '#5f005f', '#5f0087', '#5f00af', '#5f00d7', '#5f00ff',
    '#5f5f00', '#5f5f5f', '#5f5f87', '#5f5faf', '#5f5fd7', '#5f5fff',
    '#5f8700', '#5f875f', '#5f8787', '#5f87af', '#5f87d7', '#5f87ff',
    '#5faf00', '#5faf5f', '#5faf87', '#5fafaf', '#5fafd7', '#5fafff',
    '#5fd700', '#5fd75f', '#5fd787', '#5fd7af', '#5fd7d7', '#5fd7ff',
    '#5fff00', '#5fff5f', '#5fff87', '#5fffaf', '#5fffd7', '#5fffff',
    '#870000', '#87005f', '#870087', '#8700af', '#8700d7', '#8700ff',
    '#875f00', '#875f5f', '#875f87', '#875faf', '#875fd7', '#875fff',
    '#878700', '#87875f', '#878787', '#8787af', '#8787d7', '#8787ff',
    '#87af00', '#87af5f', '#87af87', '#87afaf', '#87afd7', '#87afff',
    '#87d700', '#87d75f', '#87d787', '#87d7af', '#87d7d7', '#87d7ff',
    '#87ff00', '#87ff5f', '#87ff87', '#87ffaf', '#87ffd7', '#87ffff',
    '#af0000', '#af005f', '#af0087', '#af00af', '#af00d7', '#af00ff',
    '#af5f00', '#af5f5f', '#af5f87', '#af5faf', '#af5fd7', '#af5fff',
    '#af8700', '#af875f', '#af8787', '#af87af', '#af87d7', '#af87ff',
    '#afaf00', '#afaf5f', '#afaf87', '#afafaf', '#afafd7', '#afafff',
    '#afd700', '#afd75f', '#afd787', '#afd7af', '#afd7d7', '#afd7ff',
    '#afff00', '#afff5f', '#afff87', '#afffaf', '#afffd7', '#afffff',
    '#d70000', '#d7005f', '#d70087', '#d700af', '#d700d7',
]

// From https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array#6274398
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}


// randomowe dane
let dane = [
    {
        name: 'LekA',
        form: 'tabl. powl.',
        dose: '50 mg.',
        package: '50 szt.',
        gov: [
            {
                year:2018,
                months: [
                    {
                        month: 1,
                        retailprice: 8.00,
                        supplement: 7.00
                    },
                    {
                        month: 3,
                        retailprice: 9.00,
                        supplement: 7.00
                    },
                    {
                        month: 5,
                        retailprice: 9.00,
                        supplement: 8.00
                    },
                    {
                        month: 7,
                        retailprice: 10.00,
                        supplement: 8.00
                    },
                    {
                        month: 9,
                        retailprice: 10.00,
                        supplement: 7.00
                    },
                    {
                        month: 11,
                        retailprice: 9.00,
                        supplement: 8.00
                    }
                ]
            },
            {
                year:2019,
                months: [
                    {
                        month: 1,
                        retailprice: 8.00,
                        supplement: 7.00
                    },
                    {
                        month: 3,
                        retailprice: 9.00,
                        supplement: 7.00
                    },
                    {
                        month: 5,
                        retailprice: 9.00,
                        supplement: 8.00
                    },
                    {
                        month: 7,
                        retailprice: 10.00,
                        supplement: 8.00
                    },
                    {
                        month: 9,
                        retailprice: 10.00,
                        supplement: 7.00
                    },
                    {
                        month: 11,
                        retailprice: 9.00,
                        supplement: 8.00
                    }
                ]
            }
        ],
        nfz: [
            {
                year:2018,
                semesters: [
                    {
                        semester: 1,
                        height: 300000.00
                    },
                    {
                        semester: 2,
                        height: 350000.00
                    }
                ]
            },
            {
                year:2019,
                semesters: [
                    {
                        semester: 1,
                        height: 300000.00
                    },
                    {
                        semester: 2,
                        height: 350000.00
                    }
                ]
            }
        ]
    }
];


let spis = document.getElementById('spis'); // odwołanie do tabeli legendy

// baza kolorów do legendy – 12 podstawowych, kolejność reszty losowa
colors = basicColorTable.concat(shuffle(nbColorTable)); 

let allsold = new Array(4).fill(null);  // łączna liczba sprzedanych opakowań

// tu zbieramy wszystko o lekach wraz z przydzielonymi parametrami
let chartdata = new Array(dane.length).fill(null); 

// liczenie wartości oprócz udziału w rynku
let i = 0;
while (i < dane.length) {
    let medlabel = `${dane[i].name}, ${dane[i].form}, ${dane[i].dose}, ${dane[i].package}`;
    let nfzpaydata = new Array(12).fill(null);
    let suppldata = new Array(12).fill(null);
    let piecessold = new Array(4).fill(0);
    dane[i].gov.forEach(element => {
        let yr = element.year;
        element.months.forEach(element2 => {
            let mh = element2.month;
            suppldata[6*(yr-2018)+((mh-1)/2)] = element2.supplement;
            nfzpaydata[6*(yr-2018)+((mh-1)/2)] = element2.retailprice - element2.supplement;
        });
    });
    dane[i].nfz.forEach(element => {
        let yr = element.year;
        element.semesters.forEach(element2 => {
            let sr = element2.semester;
            let pos = 2*(yr-2018)+sr - 1;
            let x = 0, s = 0;

            if (suppldata[3*pos] !== null) {
                x++;
                s += suppldata[3*pos];
            }

            if (suppldata[3*pos+1] !== null) {
                x++;
                s += suppldata[3*pos+1];
            }

            if (suppldata[3*pos+2] !== null) {
                x++;
                s += suppldata[3*pos+2];
            }

            if (x !== 0) {
                s /= x;
                if (element2.height !== null) {
                    piecessold[pos] = Math.round(element2.height / s);
                    allsold[pos] += Math.round(element2.height / s);
                }
            }
        });
    });
    let row =  spis.insertRow(i);

    let cell1 = row.insertCell(0);
    let cell2 = row.insertCell(1);
    cell1.innerHTML = `<input type="checkbox" onclick=checkVal(${i})>`
    cell2.innerHTML = medlabel;

    chartdata[i] = {
        name: medlabel, // nazwa leku
        tablerow: row, // odwołanie do rzędu legendy
        color: colors[i], // kolor przedmiotu (dotyczy wykresów liniowych)
        nfzpay: nfzpaydata, // dopłata NFZ/paczka
        supplement: suppldata, // dopłata świadczeniobiorcy/paczka
        piecessold: piecessold, // liczba sprzedanych paczek
    };

    i++;
}

// dane do wykresu kołowego (id i udział w l. paczek) będą w czterech tabelach
let parts = new Array(4).fill(null);

i = 0;
while (i < 4) {
    parts[i] = new Array(dane.length).fill(null);
    i++;
}

// przeliczenie udziału w rynku
i = 0;
while (i < dane.length) {
    let j = 0;

    while (j < 4) {
        if (allsold[j] > 0) {
            
            parts[j][i] = {
                id: i,
                val: 100 * (chartdata[i].piecessold[j]) / allsold[j],
                name: chartdata[i].name,
            };
            
        }
        j++;
    }

    i++;
}

// osie x-ów

const govlabel = ['sty 2018', 'mar 2018', 'maj 2018', 'lip 2018',
                'wrz 2018', 'lis 2018', 'sty 2019', 'mar 2019',
                'maj 2019', 'lip 2019', 'wrz 2019', 'lis 2019']

const nfzlabel = ['cze 2018', 'gru 2018', 'cze 2019', 'gru 2019'];

// tworzenie wykresu liniowego (type: nfzpay/supplement/piecessold)
function createLineChart(type) {
    let datas = new Array(dane.length).fill(null);
    
    // w zależności od wyświetlanych danych różny rozstaw
    let lbl = type === 'piecessold' ? nfzlabel : govlabel;

    // przypisanie danych do wykresu
    i = 0;
    while(i < dane.length) {
        datas[i] = {
            label: chartdata[i].name,
            backgroundColor: chartdata[i].color,
            borderColor: chartdata[i].color,
            data: chartdata[i][type],
            spanGaps: true,
            fill: false,
            lineTension: 0, // punkty będą połączone prostymi odcinkami
            hidden: true,
        };
        i++;
    }

    // generowanie wykresu
    let ctx = document.getElementById("chart").getContext("2d");
    window.weightChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels:lbl,
            datasets: datas,
            
        },
        options: weightChartLineOptions
    });

    // generowanie legendy
    document.getElementById("legend").innerHTML = weightChart.generateLegend();

    for(let i=0; i < hide.length; i++) {
        if (hide[i] === false) {
            let ch = document.querySelector(`input[data-ch-id='${i}']`);
            ch.click();
        }
            
    }

}

function createDoughnutChart(year, sem) {
    let datas = new Array(dane.length).fill(null);
    let colos = new Array(dane.length).fill(null);
    let labes = new Array(dane.length).fill(null);
    
    let pos = 2*(year - 2018) + sem - 1;
    
    // wyświetlamy tylko top 7
    let chosen = find7Best(parts[pos]);
    //chosen.sort(function(a,b) {return a.id - b.id});
    

    // przygotowujemy dane
    i = 0, sum = 0;
    while(i < chosen.length) {
        datas[i] = chosen[i].val;
        sum += datas[i];
        colos[i] = colors[i];
        labes[i] = chosen[i].name;
        i++;
    }

    // ewentualna reszta w pozostałych
    if (chosen.length == 7 && sum < 100.00) {
        datas.push(100.00 - sum);
        colos.push(colors[7]);
        labes.push('Pozostałe');
    }
    
    // i wykres
    new Chart(document.getElementById('chart'), {
        type: 'doughnut',
        data:{
            label: 'Udział w rynku w proc.',
            datasets: [{
                data: datas,
                backgroundColor: colos,
            }],
            labels: labes,
        },
        options: weightChartDoughnutOptions
    });

    let legendHtml = [];

    for (let i=0; i<chosen.length; i++) {
        legendHtml.push(`<tr>
        <td style="background-color:${colos[i]};" data-id='${i}'></td>
        <td>${labes[i]}</td>
        </tr>`);
                                                                        
    }

    document.getElementById("spis").innerHTML = legendHtml;
}

// // szablon tworzenia wykresu liniowego
// new Chart(document.getElementById('chart'),{
//     type: 'line',
//     data: {
//         labels:['cze 2018', 'gru 2018', 'cze 2019', 'gru 2019'],
//         datasets:[
//             {

//                 label:'LekA',
//                 backgroundColor: '#FFFF00',
//                 borderColor: '#FFFF00',
//                 data: [7.00, null, 10.00, 9.00],
//                 spanGaps: true,
//                 fill: false,
//                 lineTension: 0,
//                 hidden: true,
//             },
//             {
//                 label:'LekB',
//                 backgroundColor: '#FF0000',
//                 borderColor: '#FF0000',
//                 data: [10.00, 9.00, 7.00, null],
//                 spanGaps: true,
//                 fill: false,
//                 lineTension: 0,
//                 hidden: true,
//             },
//             {
//                 label:'LekC',
//                 backgroundColor: '#0000FF',
//                 borderColor: '#0000FF',
//                 data: [13.00, 13.00, 12.00, 6.00],
//                 spanGaps: true,
//                 fill: false,
//                 lineTension: 0,
//                 hidden: true,
//             },
//             {
//                 label:'LekD',
//                 backgroundColor: '#FF00FF',
//                 borderColor: '#FF00FF',
//                 data: [10.00, 9.00, 9.00, 10.00],
//                 spanGaps: true,
//                 fill: false,
//                 lineTension: 0,
//                 hidden: true,  
//             },
//         ],
        
//     },
//     options: weightChartLineOptions
// });

// console.log(chartdata)

// // szablon tworzenia wykresu kołowego
// new Chart(document.getElementById('chart2'),{
//     type: 'doughnut',
//     data:{
//         label: 'Udział w rynku w proc.',
//         datasets: [{
//             data: [37, 63],
//             backgroundColor: [
//                 'red', 'green', 'blue'
//             ],
//         }],
//         labels: [
//             'LekA',
//             'LekB'
//         ]
//     },
//     options: {
//         legend: {
//            display: false
//         },
//     }
// });                                                                

// let ctx = document.getElementById("chart").getContext("2d");
// window.weightChart = new Chart(ctx, {
//     type: 'line',
//     data: {
//         labels:['cze 2018', 'gru 2018', 'cze 2019', 'gru 2019'],
//         datasets:[
//             {

//                 label:'LekA',
//                 backgroundColor: colors[0],
//                 borderColor: colors[0],
//                 data: [7.00, null, 10.00, 9.00],
//                 spanGaps: true,
//                 fill: false,
//                 lineTension: 0,
//                 hidden: true,
//             },
//             {
//                 label:'LekB',
//                 backgroundColor: colors[1],
//                 borderColor: colors[1],
//                 data: [10.00, 9.00, 7.00, null],
//                 spanGaps: true,
//                 fill: false,
//                 lineTension: 0,
//                 hidden: true,
//             },
//             {
//                 label:'LekC',
//                 backgroundColor: colors[2],
//                 borderColor: colors[2],
//                 data: [13.00, 13.00, 12.00, 6.00],
//                 spanGaps: true,
//                 fill: false,
//                 lineTension: 0,
//                 hidden: true,
//             },
//             {
//                 label:'LekD',
//                 backgroundColor: colors[3],
//                 borderColor: colors[3],
//                 data: [10.00, 9.00, 9.00, 10.00],
//                 spanGaps: true,
//                 fill: false,
//                 lineTension: 0,
//                 hidden: true,  
//             },
//         ],
        
//     },
//     options: weightChartLineOptions
// });
// document.getElementById("legend").innerHTML = weightChart.generateLegend();

document.getElementById("b1").addEventListener('click', ()=>{
    for(let i=0; i < hide.length; i++) {
        if (hide[i] === true) {
            hide[i] = false;
            let ch = document.querySelector(`input[data-ch-id='${i}']`);
            ch.click();
        }
            
    }
});

document.getElementById("b2").addEventListener('click', ()=>{
    for(let i=0; i < hide.length; i++) {
        if (hide[i] === false) {
            hide[i] = true;
            let ch = document.querySelector(`input[data-ch-id='${i}']`);
            ch.click();
        }
            
    }
});

// createDoughnutChart(2018, 1);

document.querySelector("#piecbut").addEventListener('click', () => {
    createLineChart('piecessold');
    document.getElementById("doughnutchoose").setAttribute('display', 'none');
    document.querySelectorAll("#doughnutchoose input").forEach(element => {
        element.setAttribute('disabled', 'disabled');
        document.getElementById("b1").removeAttribute('disabled');
        document.getElementById("b2").removeAttribute('disabled');
    });
});
document.querySelector("#suplbut").addEventListener('click', () => {
    createLineChart('supplement')
    document.querySelectorAll("#doughnutchoose input").forEach(element => {
        element.setAttribute('disabled', 'disabled');
        document.getElementById("b1").removeAttribute('disabled');
        document.getElementById("b2").removeAttribute('disabled');
    });
});
document.querySelector("#nfzbut").addEventListener('click', () => {
    createLineChart('nfzpay');
    document.querySelectorAll("#doughnutchoose input").forEach(element => {
        element.setAttribute('disabled', 'disabled');
        document.getElementById("b1").removeAttribute('disabled');
        document.getElementById("b2").removeAttribute('disabled');
    });
});

document.querySelector("#partbut").addEventListener('click', () => {
    createDoughnutChart(2019, 2);
    document.querySelectorAll("#doughnutchoose input").forEach(element => {
        element.removeAttribute('disabled');
        document.getElementById("b1").setAttribute('disabled', 'disabled');
        document.getElementById("b2").setAttribute('disabled', 'disabled');
    });
});

document.getElementById("d20192").addEventListener('click', () => {
    createDoughnutChart(2019, 2);
});

document.getElementById("d20191").addEventListener('click', () => {
    createDoughnutChart(2019, 1);
});

document.getElementById("d20182").addEventListener('click', () => {
    createDoughnutChart(2019, 2);
});

document.getElementById("d20181").addEventListener('click', () => {
    createDoughnutChart(2018, 1);
});

createLineChart('nfzpay');