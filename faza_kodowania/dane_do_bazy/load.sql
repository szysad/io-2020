DROP TABLE IF EXISTS refundation;
DROP TABLE IF EXISTS medicine;
DROP TABLE IF EXISTS refundamount;

--Wykaz danych dot. refundacji leków z apteki
CREATE TABLE refundation(id INTEGER NOT NULL PRIMARY KEY, year INTEGER, month INTEGER, ean TEXT, retailprice NUMERIC(10, 2), supplement NUMERIC(10,2), UNIQUE(year, month, ean, supplement));


--Informacja o leku
CREATE TABLE medicine(id INTEGER PRIMARY KEY, ean TEXT UNIQUE, name TEXT, form TEXT, dose TEXT, international TEXT, package TEXT);

--Sumaryczna wysokość refundacji
CREATE TABLE refundamount(id INTEGER NOT NULL PRIMARY KEY, year INTEGER, semester INTEGER, ean TEXT, height NUMERIC(10, 2), UNIQUE(year, semester, ean));

.separator "\t"
.import refundamount.txt refundamount
.import medicine.txt medicine
.import refundation.txt refundation
