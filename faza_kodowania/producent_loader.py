'''this module reads producent data and adds it to database'''
#!/usr/bin/env python3

import logging
import math
from drug_search.models import Producent, Medicine

PRODUCENT_F_DATA = '../dane_do_bazy/np.txt'
PER_PERCENT_PROGRESS_DISPLAY = 0.1
LOG_FILE = 'producent.log'
ARGS_IN_LINE = 2
EAN_LEN = 14
ERRORS = 0

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO, filename=LOG_FILE)

with open(PRODUCENT_F_DATA, 'r') as f:
    lines = f.readlines()
    PER_DISPLAY = math.floor(len(lines) * PER_PERCENT_PROGRESS_DISPLAY)
    for i, line in enumerate(lines):
        if i % PER_DISPLAY == 0:
            print("done ... {:.2f}%".format(i * 100 / len(lines)))
        args = line.split('|')
        if len(args) != ARGS_IN_LINE:
            ERRORS += 1
            logging.warning('line: %s content:%s', i, repr(line))
            continue
        for it, val in enumerate(args):
            args[it] = val.strip()
        if not args[0].isnumeric():
            ERRORS += 1
            logging.warning('line: %s content:%s', i, repr(line))
            continue
        if len(args[0]) < EAN_LEN:
            len_diff = EAN_LEN - len(args[0])
            args[0] = '{zeros}{org}'.format(zeros='0' * len_diff, org=args[0])
        elif len(args[0]) > EAN_LEN:
            ERRORS += 1
            logging.warning('line: %s content:%s', i, repr(line))
            continue
        p = Producent(name=args[1])
        meds = Medicine.objects.filter(ean=args[0])
        if len(meds) == 0:
            ERRORS += 1
            logging.error('(ean, producent) = (%s, %s) does not match any medicine', *args)
            continue
        p.save()
        for m in meds:
            m.producent = p
            m.save()
    logging.info("total numer of errors: %s", ERRORS)
