#!/usr/bin/env python3

from drug_search.models import Medicine, Refundamount, Refundation

medicine_f_path = '../dane_do_bazy/medicine.txt'
refundation_f_path = '../dane_do_bazy/refundation.txt'
refundamount_f_path = '../dane_do_bazy/refundamount.txt'
show_progress_after = 100

'''
#load medicines without ean duplicates
f = open(medicine_f_path, 'r')
lines = f.readlines()
lines_numb = len(lines)
print('starting to process medicines file without ean duplicates. {} lines to go'.format(lines_numb))

count = 1
eans = set()
for line in lines:
    args = line.split('\t')
    assert len(args) == 7
    if args[1] not in eans:
        count = count + 1
        eans.add(args[1])
        #print("id: {0}, ean: {1}, name: {2}, form: {3}, dose: {4}, international: {5}, package: {6}".format(args[0], args[1], args[2], args[3], args[4], args[5], args[6]))
        m = Medicine(ean=args[1], name=args[2], form=args[3], dose=args[4], international=args[5], package=args[6])
        m.save()
    if count % show_progress_after == 0:
        percent = (count / lines_numb) * 100
        print('status: {0} out of {1}, {2}%'.format(count, lines_numb, round(percent, 2)))

f.close()
print('succesfully processed whole medicines file. {} new objects added to database'.format(count))


#load refundation
f = open(refundation_f_path, 'r')
lines = f.readlines()
lines_numb = len(lines)
print('starting to process refundation files. {} lines to go'.format(lines_numb))

count = 1
for line in lines:
    args = line.split('\t')
    assert len(args) == 6
    #print("id:{0}, year: {1}, month:{2}, ean:{3}, retailprice:{4}, supplement:{5}".format(args[0], args[1], args[2], args[3], args[4], args[5]))
    meds = Medicine.objects.filter(ean=args[3])
    assert len(meds) == 1
    for med in meds:
        count = count + 1
        r = Refundation(year=args[1], month=args[2], retail_price=args[4], supplement=args[5], medicine=med)
        r.save()

    if count % show_progress_after == 0:
        percent = (count / lines_numb) * 100
        print('status: {0} out of {1}, {2}%'.format(count, lines_numb, round(percent, 2)))

f.close()
print('succesfully processed whole refundation file. {} new objects added to database'.format(count))
'''

Refundamount.objects.all().delete()

#load refundamount
f = open(refundamount_f_path, 'r')
lines = f.readlines()
lines_numb = len(lines)
print('starting to process refundamout file. {} lines to go.'.format(lines_numb))

count = 1
nomatch = 0
for line in lines:
    args = line.split('\t')
    if len(args) != 5:
        print(args)
        print()
        assert False
    #print("id:{0}, year:{1}, semester:{2}, ean:{3}, height:{4}".format(args[0], args[1], args[2], args[3], args[4]))
    meds = Medicine.objects.filter(ean=args[3])
    if len(meds) > 1:
        print(meds)
        assert False
    if len(meds) == 0:
        nomatch = nomatch + 1
    for med in meds:
        count = count + 1
        r = Refundamount(year=args[1], semester=args[2], height=args[4], applies_to=med)
        r.save()

    if count % show_progress_after == 0:
        percent = (count / lines_numb) * 100
        print('status: {0} out of {1}, {2}%'.format(count, lines_numb, round(percent, 2)))

f.close()
print('succesfully processed whole refundamount file. {} new objects added to database'.format(count))
print('nomatch = {}'.format(nomatch))


