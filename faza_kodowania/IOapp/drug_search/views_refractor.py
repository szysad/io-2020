from .models import Medicine

def is_pos_int(val):
    try:
        per_page = int(val)
        return per_page > 0
    except ValueError:
        return False

def getMedData(medicine: Medicine):
    data = {
        'name': medicine.name,
        'form': medicine.form,
        'dose': medicine.dose,
        'package': medicine.package,
        'gov': None,
        'nfz': None,
        'producer': None,
    }
    # set producer if exists
    if medicine.producent is not None:
        data['producer'] = medicine.producent.name

    query_gov = medicine.refundation_set.values()
    data['gov'] = getGovList(query_gov)
    query_nfz = medicine.refundamount_set.values()
    data['nfz'] = getNfzList(query_nfz)
    return data


def getNfzList(nfzValues):
    years_set = nfzValues.values('year').distinct()
    nfz_list = []
    for year in years_set:
        nfz_elem = {
            'year': year['year'],
            'semesters': []
        }
        same_year_refs = nfzValues.filter(year=year['year']).values()
        for refammount in same_year_refs:
            nfz_elem['semesters'].append({
                'semester': refammount['semester'],
                'height': refammount['height']
            })
        nfz_list.append(nfz_elem)
    return nfz_list


def getGovList(govValues):
    years_set = govValues.values('year').distinct()
    gov_list = []
    for year in years_set:
        same_year_refunds = {
            'year': year['year'],
            'months': []
        }
        same_year_refund = govValues.filter(year=year['year']).values()
        for refund in same_year_refund:
            month_refund = {
                'month': refund['month'],
                'retailprice': refund['retail_price'],
                'supplement': refund['supplement']
            }
            same_year_refunds['months'].append(month_refund)
        gov_list.append(same_year_refunds)
    return gov_list
