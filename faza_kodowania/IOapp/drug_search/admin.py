from django.contrib import admin

from .models import Medicine, Refundation, Refundamount

# Register your models here.

admin.site.register(Medicine)
admin.site.register(Refundation)
admin.site.register(Refundamount)
