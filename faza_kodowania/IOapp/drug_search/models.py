from django.db import models

# Create your models here.


class Producent(models.Model):
    '''Producent of Medicine'''
    name = models.CharField(max_length=96)

class Medicine(models.Model):
    """NFZ medicine"""
    ean = models.CharField(max_length=64)
    name = models.CharField(max_length=64)
    form = models.CharField(max_length=32)
    dose = models.CharField(max_length=32)
    international = models.CharField(max_length=64)
    package = models.CharField(max_length=64)
    producent = models.ForeignKey(Producent, on_delete=models.SET_NULL, null=True)

class Refundation(models.Model):
    """Medicine NFZ refundation"""
    year = models.IntegerField()
    month = models.IntegerField()
    retail_price = models.DecimalField(max_digits=10, decimal_places=2)
    supplement = models.DecimalField(max_digits=10, decimal_places=2)
    medicine = models.ForeignKey(Medicine, on_delete=models.CASCADE, null=True)

class Refundamount(models.Model):
    """Goverment refundation of medicine in semestrer"""
    year = models.IntegerField()
    semester = models.IntegerField()
    height = models.DecimalField(max_digits=10, decimal_places=2)
    applies_to = models.ForeignKey(Medicine, on_delete=models.CASCADE, null=True)

