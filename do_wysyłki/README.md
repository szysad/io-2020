## How to run a server

1. Install python environment based on requirements.txt and activate it:

    $ python3 -m venv <environment_directory\>
    
    $ source <environment_directory\>/bin/activate

    $ python3 -m pip install -r requirements.txt
    
2. Go to IOapp folder and run server:
    $ python3 manage.py runserver
3. Main page of the project is under address given in command output
