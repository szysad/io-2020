from django.shortcuts import render
from django.core.paginator import Paginator
from django.db import connection
import simplejson
from .models import Medicine
from .views_refractor import is_pos_int, getMedData

def listfetchall(cursor) -> list:
    "Return all rows from a cursor as a dict"
    return [
        val[0] for val in cursor.fetchall()
    ]

def excluded_internationals() -> list:
    """returns listt with medicines with unique international name"""
    with connection.cursor() as cursor:
        cursor.execute( '''
                            SELECT t1.medicine_id 
                            FROM drug_search_refundation t1 INNER JOIN drug_search_refundation t2
                            ON t1.medicine_id = t2.medicine_id
                            AND t1.month = t2.month
                            AND t1.year = t2.year
                            WHERE t1.supplement != t2.supplement
                        ''')
        d = listfetchall(cursor)
    return d

# views functions

def handler404(request, exception):
    return render(request, 'drug_search/404.html')

def index(request):
    '''returns list of substances'''
    results_per_page = [10, 15, 20, 30, 50]
    per_page = int(request.GET.get('per_page', 10))
    curr_page = request.GET.get('page', 1)
    phrase = request.GET.get('phrase', '').strip()
    sort_inc = request.GET.get('increasing', '') == 'on'

    if per_page not in results_per_page:
        per_page = min(results_per_page)

    if not is_pos_int(curr_page):
        curr_page = 1

    exclude_med_ids = excluded_internationals()
    by_international = Medicine.objects.filter(
        international__icontains=phrase).exclude(
            id__in=exclude_med_ids).values_list('international', flat=True)

    substance_qset = by_international.distinct()
    #sort descending by deafult, if sort_inc is true then asc
    substance_qset = substance_qset.order_by('{}international'.format('' if sort_inc else '-'))

    paginator = Paginator(substance_qset, per_page)
    context = {
        'per_page': per_page,
        'sort_inc': sort_inc,
        'phrase': phrase,
        'results_per_page': results_per_page,
        'page_obj': paginator.get_page(curr_page),
    }
    return render(request, 'drug_search/results.html', context)

def singleSubstance(request, international):
    '''view with substance data'''
    medicines = Medicine.objects.filter(international=international)
    if len(medicines) == 0:
        return handler404(request, Exception('error'))  
    data = []
    for med in medicines:
        data.append(getMedData(med))

    context = {
        'data': simplejson.dumps(data),
        'substance': international
    }
    return render(request, 'drug_search/chart.html', context)
