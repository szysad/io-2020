from django.urls import path
from django.urls import reverse_lazy
from django.views.generic import RedirectView
from . import views


urlpatterns = [
    path('', RedirectView.as_view(url=reverse_lazy('drug-search'), permanent=True)),
    path('drug_search/', RedirectView.as_view(url=reverse_lazy('drug-search'), permanent=True)),
    path('drug_search', views.index, name='drug-search'),
    path('drug_search/<str:international>', views.singleSubstance, name='substance-page')
]

