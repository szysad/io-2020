
dane.forEach(element => {
    if (element.producer === null)
        element.producer = 'Producent nieznany';
});

dane.sort(alphaorder);


// tablica informująca, czy n-ty element jest wyświetlany 
let hide = null;

// informacje o aktualnym semestrze i roku (do wykresu kołowego)
let year = 2019;
let sem = 2;
let group = 'name';

// zmienne pomocnicze
let typ = 'line' // aktualny typ wykresu
let lock = true; // jeżeli true, to udajemy, że group='single'
let lastGrp = 'single'; // jak ostatnio był grupowany wykres

// ostatnio kliknięty element – dane w postaci struktury
let lastClick = {
    type: null,
    prod: null,
    name: null,
    count: 0
}

// ostatni wczytany element – konkretnie nazwa i producent
// potrzebne do tworzenia tabel nazw i producentów
let lastRead = {
    prod: null,
    name: null
}

// baza kolorów do legendy – 12 podstawowych, kolejność reszty losowa
let colors = basicColorTable.concat(shuffle(nbColorTable)); 

// Rozwiązanie na podstawie https://github.com/chartjs/Chart.js/issues/2565
const weightChartLineOptions = (title, grp) => {
    let scal = null

    return {
    title: {
        display: true,
        text: `${title}`
    },
    responsive: true,
    legendCallback: function(chart) {
        let newChart;
        let legendHtml = [];
        legendHtml.push('<table id="spis">');
        legendHtml.push('<tbody style="display:flex;flex-direction:column;">');

        if (hide === null) {
            hide = new Array(0);
            newChart = true;
        } else {
            newChart = false;
        }

        // tworzymy odpowiednią legendę w zależności od tego, po czym grupujemy
        for (let i=0; i<chart.data.datasets.length; i++) {
            if (grp === 'single') {
                legendHtml.push(`<tr style="order:2;" data-tr-id='${i}'>
                <td data-id='${i}'><input type="checkbox" data-ch-id='${i}' onclick="updateDataset(event, ${i}, ` + `\'` + chart.legend.legendItems[i].datasetIndex + `\'` + `)"></td>
                <td onclick="clickProd(${i})" style="cursor:pointer;">${chartdata[i].producer}</td>
                <td onclick="clickName(${i})" style="cursor:pointer;">${chartdata[i].firstname}</td>
                <td>${chartdata[i].lastname}</td>
                </tr>`);   
            } else if (grp === 'name') {
                legendHtml.push(`<tr style="order:2;" data-tr-id='${i}'>
                <td data-id='${i}'><input type="checkbox" data-ch-id='${i}' onclick="updateDataset(event, ${i}, ` + `\'` + chart.legend.legendItems[i].datasetIndex + `\'` + `)"></td>
                <td onclick="clickProd(${i})" style="cursor:pointer">${namestats[i].prod}</td>
                <td>${namestats[i].name}</td>
                </tr>`);
            } else if (grp === 'prod') {
                legendHtml.push(`<tr style="order:2;" data-tr-id='${i}'>
                <td data-id='${i}'><input type="checkbox" data-ch-id='${i}' onclick="updateDataset(event, ${i}, ` + `\'` + chart.legend.legendItems[i].datasetIndex + `\'` + `)"></td>
                <td>${prodstats[i].prod}</td>
                </tr>`);
            }
               
            if (newChart === true) {
                hide.push(true);
            }                                     
        }                                                                                  

        legendHtml.push('</tbody>');
        legendHtml.push('</table>');                                              
        return legendHtml.join("");                                                        
    },                                                                                     
    legend: {                                                                              
        display: false                                                                     
    },
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }]
    }                                                                                      
}};                

// Rozwiązanie z https://github.com/chartjs/Chart.js/issues/2565
const weightChartDoughnutOptions = (mies, year) => {return {
    responsive: true,
    title: {
        display: true,
        text: `Udział w rynku – ${mies} ${year} w %`
    },
    legendCallback: function(chart) {
        let legendHtml = [];
        legendHtml.push('<table id="spis">');
        legendHtml.push('<tbody style="display:flex;flex-direction:column;">');

        for (let i=0; i<chart.data.datasets[0].data.length; i++) {
            legendHtml.push(`<tr style="order:2;" data-tr-id='${i}'>
            <td style="background-color:${chart.data.datasets[0].backgroundColor[i]};" data-id='${i}'></td>
            <td>${chart.data.labels[i]}</td>
            </tr>`);
                                                                          
        }                                                                                  
                              
        legendHtml.push('</tbody>');                                
        legendHtml.push('</table>');                                                       
        return legendHtml.join("");                                                        
    },                                                                                     
    legend: {                                                                              
        display: false                                                                     
    }
}};    

// Show/hide chart by click legend
updateDataset = function(e, id, datasetIndex) {
    lastClick.type = 'single';
    var index = datasetIndex;
    var ci = e.view.weightChart;
    var meta = ci.getDatasetMeta(index);

    // See controller.isDatasetVisible comment
    if (meta.hidden === null) {
        meta.hidden = !ci.data.datasets[index].hidden;
        hide[id] = false;
        document.querySelector(`tr[data-tr-id='${id}']`).style.order = 1;
        document.querySelector(`td[data-id='${id}']`).style.backgroundColor=`${ci.data.datasets[id].backgroundColor}`;
        document.querySelector(`input[data-ch-id='${id}']`).checked = true;
    } else {
        meta.hidden = null;
        hide[id] = true;
        document.querySelector(`tr[data-tr-id='${id}']`).style.order = 2;
        document.querySelector(`td[data-id='${id}']`).style.backgroundColor='';
        document.querySelector(`input[data-ch-id='${id}']`).checked = false;
    }

    // We hid a dataset ... rerender the chart
    ci.update();
};

let allsold = new Array(4).fill(null);  // łączna liczba sprzedanych opakowań

// tu zbieramy wszystko o lekach wraz z przydzielonymi parametrami
let chartdata = new Array(dane.length).fill(null); 

// tu zbieramy informacje o grupach leków
let prodstats = new Array(0);
let namestats = new Array(0);

// liczenie wartości oprócz udziału w rynku
let i = 0;
while (i < dane.length) {

    if (dane[i].producer !== lastRead.prod) {
        let j = prodstats.length;
        prodstats.push({
            color: colors[j],
            prod: dane[i].producer,
            heights: new Array(4).fill(0),
        });
    }

    if (dane[i].producer !== lastRead.prod || dane[i].name !== lastRead.name) {
        let j = namestats.length;
        namestats.push({
            color: colors[j],
            prod: dane[i].producer,
            name: dane[i].name,
            heights: new Array(4).fill(0),
        })
    }

    let medlabel = `${dane[i].producer}, ${dane[i].name}, ${dane[i].form}, ${dane[i].dose}, ${dane[i].package}`;
    let nfzpaydata = new Array(12).fill(null);
    let suppldata = new Array(12).fill(null);
    let piecessold = new Array(4).fill(0);
    dane[i].gov.forEach(element => {
        let yr = element.year;
        element.months.forEach(element2 => {
            let mh = element2.month;
            suppldata[6*(yr-2018)+((mh-1)/2)] = element2.supplement;

            if (element2.supplement !== null) {
                nfzpaydata[6*(yr-2018)+((mh-1)/2)] = element2.retailprice - element2.supplement;
            } else {
                nfzpaydata[6*(yr-2018)+((mh-1)/2)] = null;
            }
        });
    });
    dane[i].nfz.forEach(element => {
        let yr = element.year;
        element.semesters.forEach(element2 => {
            let sr = element2.semester;
            let pos = 2*(yr-2018)+sr - 1;
            let x = 0, s = 0;

            if (nfzpaydata[3*pos] !== null) {
                x++;
                s += nfzpaydata[3*pos];
                nfzpaydata[3*pos] = nfzpaydata[3*pos].toFixed(2);
                suppldata[3*pos] = suppldata[3*pos].toFixed(2);
            }

            if (nfzpaydata[3*pos+1] !== null) {
                x++;
                s += nfzpaydata[3*pos+1];
                nfzpaydata[3*pos+1] = nfzpaydata[3*pos+1].toFixed(2);
                suppldata[3*pos+1] = suppldata[3*pos+1].toFixed(2);
            }

            if (nfzpaydata[3*pos+2] !== null) {
                x++;
                s += nfzpaydata[3*pos+2];
                nfzpaydata[3*pos+2] = nfzpaydata[3*pos+2].toFixed(2);
                suppldata[3*pos+2] = suppldata[3*pos+2].toFixed(2);
            }

            if (x !== 0) {
                s /= x;
                if (element2.height !== null) {

                    if (s !== 0) {
                        prodstats[prodstats.length - 1].heights[pos] += Math.round(element2.height / s);
                        namestats[namestats.length - 1].heights[pos] += Math.round(element2.height / s);
                        piecessold[pos] = Math.round(element2.height / s);
                        allsold[pos] += Math.round(element2.height / s);
                    } else {
                        piecessold[pos] = 0;
                    }
                    
                }
            }
        });
    });
    let row =  spis.insertRow(i);

    let cell1 = row.insertCell(0);
    let cell2 = row.insertCell(1);
    cell1.innerHTML = `<input type="checkbox" onclick=checkVal(${i})>`
    cell2.innerHTML = medlabel;

    chartdata[i] = {
        producer: dane[i].producer,
        firstname: dane[i].name,
        lastname: `${dane[i].form}, ${dane[i].dose}, ${dane[i].package}`,
        name: medlabel, // nazwa leku
        tablerow: row, // odwołanie do rzędu legendy
        color: colors[i], // kolor przedmiotu (dotyczy wykresów liniowych)
        nfzpay: nfzpaydata, // dopłata NFZ/paczka
        supplement: suppldata, // dopłata świadczeniobiorcy/paczka
        piecessold: piecessold, // liczba sprzedanych paczek
    };

    lastRead = {
        prod: dane[i].producer,
        name: dane[i].name
    };

    i++;
}

// dane do wykresu kołowego (id i udział w l. paczek) będą w czterech tabelach
let parts = new Array(4).fill(null);
let prodparts = new Array(4).fill(null);
let nameparts = new Array(4).fill(null);

i = 0;
while (i < 4) {
    parts[i] = new Array(dane.length).fill(null);
    prodparts[i] = new Array(prodstats.length).fill(null);
    nameparts[i] = new Array(namestats.length).fill(null);
    i++;
}

// przeliczenie udziału w rynku dla pojedynczych bytów
i = 0;
while (i < dane.length) {
    let j = 0;

    while (j < 4) {
        if (allsold[j] > 0) {
            
            parts[j][i] = {
                id: i,
                val: 100 * (chartdata[i].piecessold[j]) / allsold[j],
                name: chartdata[i].name,
            };
            
        } else {
            parts[j][i] = {
                id: i,
                val: 0,
                name: chartdata[i].name,
            };
        }
        j++;
    }

    i++;
}

// przeliczenie udziału w rynku producentów
i = 0;
while (i < prodstats.length) {
    let j = 0;

    while (j < 4) {
        if (allsold[j] > 0) {
            
            prodparts[j][i] = {
                id: i,
                color: prodstats[i].color,
                val: 100 * (prodstats[i].heights[j]) / allsold[j],
                prod: prodstats[i].prod
            };
            
        } else {
            prodparts[j][i] = {
                id: i,
                color: prodstats[i].color,
                val: 0,
                prod: prodstats[i].prod
            };
        }
        j++;
    }

    i++;
}

// przeliczenie udziału w rynku leków o tej samej nazwie
i = 0;
while (i < namestats.length) {
    let j = 0;

    while (j < 4) {
        if (allsold[j] > 0) {
            nameparts[j][i] = {
                id: i,
                color: namestats[i].color,
                val: 100 * (namestats[i].heights[j]) / allsold[j],
                prod: namestats[i].prod,
                name: namestats[i].name
            };
            
        } else {
            nameparts[j][i] = {
                id: i,
                color: namestats[i].color,
                val: 0,
                prod: namestats[i].prod,
                name: namestats[i].name
            };
        }
        j++;
    }

    i++;
}

// tworzenie wykresu liniowego (type: nfzpay/supplement/piecessold)
createLineChart = (type) => {
    typ = 'line';
    let datas;
    let lbl;
    let title;
    let grp;

    if (type !== 'piecessold' || group === 'single') {
        grp = 'single';
        lock = true;
        datas = new Array(dane.length).fill(null);
    
        // w przypadku zmiany grupowania, hide musi zostać utowrzony na nowo
        if (grp !== lastGrp) {
            lastGrp = grp;
            hide = null;
        }

        // w zależności od wyświetlanych danych różny rozstaw
        lbl = type === 'piecessold' ? nfzlabel : govlabel;
    
        // nadanie tytułu
        if (type === 'piecessold') title = 'Liczba sprzedanych paczek';
        if (type === 'nfzpay') title = 'Dopłata NFZ';
        if (type === 'supplement') title = 'Dopłata świadczeniobiorcy';
    
        // przypisanie danych do wykresu
        i = 0;
        while(i < dane.length) {
            datas[i] = {
                label: chartdata[i].name,
                backgroundColor: chartdata[i].color,
                borderColor: chartdata[i].color,
                data: chartdata[i][type],
                spanGaps: true,
                fill: false,
                lineTension: 0, // punkty będą połączone prostymi odcinkami
                hidden: true,
            };
            i++;
        }
    } else if (group === 'name') {
        grp = 'name';
        lock = false;
        if (grp !== lastGrp) {
            lastGrp = grp;
            hide = null;
        }
        
        datas = new Array(namestats).fill(null);
        lbl = nfzlabel;
        title = 'Liczba sprzedanych paczek';

        i = 0;
        while(i < namestats.length) {
            datas[i] = {
                label: `${namestats[i].prod}, ${namestats[i].name}`,
                backgroundColor: namestats[i].color,
                borderColor: namestats[i].color,
                data: namestats[i].heights,
                spanGaps: true,
                fill: false,
                lineTension: 0, // punkty będą połączone prostymi odcinkami
                hidden: true,
            };
            i++;
        }
    } else if (group === 'prod') {
        lock = false;
        grp = 'prod';
        
        if (grp !== lastGrp) {
            lastGrp = grp;
            hide = null;
        }
        
        datas = new Array(prodstats).fill(null);
        lbl = nfzlabel;
        title = 'Liczba sprzedanych paczek';
        
        i = 0;
        while(i < prodstats.length) {
            datas[i] = {
                label: prodstats[i].prod,
                backgroundColor: prodstats[i].color,
                borderColor: prodstats[i].color,
                data: prodstats[i].heights,
                spanGaps: true,
                fill: false,
                lineTension: 0, // punkty będą połączone prostymi odcinkami
                hidden: true,
            };
            i++;
        }
    }

    cleanCanvasContainer();
    
    let ctx = document.getElementById("chart").getContext("2d");
    window.weightChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels:lbl,
            datasets: datas,
            
        },
        options: weightChartLineOptions(`${title}`, grp)
    });

    // generowanie legendy
    document.getElementById("legend").innerHTML = weightChart.generateLegend();
    
    for(let i=0; i < hide.length; i++) {
        if (hide[i] === false) {
            let ch = document.querySelector(`input[data-ch-id='${i}']`);
            ch.click();
        }
    }

    document.getElementById("hidedoughnut").style.display = 'none';
}

createDoughnutChart = () => {
    lock = false;
    typ = 'doughnut'
    let pos = 2*(year - 2018) + sem - 1;

    // nadanie tytułu
    let mies;
    if (sem === 1) mies = 'czerwiec'
    else mies = 'grudzień'
    let datas;
    let colos;
    let labes;

    if (group === 'single') {
        datas = new Array(min(dane.length, 7)).fill(null);
        colos = new Array(min(dane.length, 7)).fill(null);
        labes = new Array(min(dane.length, 7)).fill(null);
    
        // wyświetlamy tylko top 7
        let chosen = find7Best(parts[pos]);

        // przygotowujemy dane
        i = 0, sum = 0;
        while(i < chosen.length) {
            datas[i] = chosen[i].val.toFixed(2);
            sum += chosen[i].val;
            colos[i] = colors[chosen[i].id];
            labes[i] = chosen[i].name;
            i++;
        }
    
        // ewentualna reszta w pozostałych
        if (dane.length > 7) {
            datas.push((100.00 - sum).toFixed(2));
            colos.push(colors[7]);
            labes.push('Pozostałe');
        }
    } else if (group === 'name') {
        datas = new Array(min(namestats.length, 7)).fill(null);
        colos = new Array(min(namestats.length, 7)).fill(null);
        labes = new Array(min(namestats.length, 7)).fill(null);

        let chosen = nameparts[pos].sort(valorder).slice(0,7).sort(alphaorder2);

        // przygotowujemy dane
        i = 0, sum = 0;
        while(i < chosen.length) {
            datas[i] = chosen[i].val.toFixed(2);
            sum += chosen[i].val;
            colos[i] = chosen[i].color; //tymczasowo
            labes[i] = `${chosen[i].prod}, ${chosen[i].name}`;
            i++;
        }
    
        // ewentualna reszta w pozostałych
        if (namestats.length > 7) {
            datas.push((100.00 - sum).toFixed(2));
            colos.push(colors[chosen[6].id + 1]);
            labes.push('Pozostałe');
        }

    } else if (group === 'prod') {
        datas = new Array(min(prodstats.length, 7)).fill(null);
        colos = new Array(min(prodstats.length, 7)).fill(null);
        labes = new Array(min(prodstats.length, 7)).fill(null);

        let chosen = prodparts[pos].sort(valorder).slice(0,7).sort(alphaorder3);
        // przygotowujemy dane
        i = 0, sum = 0;
        while(i < chosen.length) {
            datas[i] = chosen[i].val.toFixed(2);
            sum += chosen[i].val;
            colos[i] = chosen[i].color; //tymczasowo
            labes[i] = chosen[i].prod;
            i++;
        }
    
        // ewentualna reszta w pozostałych
        if (namestats.length > 7) {
            datas.push((100.00 - sum).toFixed(2));
            colos.push(colors[chosen[6].id + 1]);
            labes.push('Pozostałe');
        }

    }

    // i wykres
    
    cleanCanvasContainer();
    
    let ctx = document.getElementById("chart").getContext("2d");
    window.weightChart = new Chart(ctx, {
        type: 'doughnut',
        data:{
            label: 'Udział w rynku w proc.',
            datasets: [{
                data: datas,
                backgroundColor: colos,
            }],
            labels: labes,
        },
        options: weightChartDoughnutOptions(mies, year)
    });

    // generowanie legendy
    document.getElementById("legend").innerHTML = weightChart.generateLegend();
    
    document.getElementById("hidedoughnut").style.display = '';
}

document.getElementById("b1").addEventListener('click', ()=>{

    for(let i=0; i < hide.length; i++) {
        if (hide[i] === true) {
            hide[i] = false;
            let ch = document.querySelector(`input[data-ch-id='${i}']`);
            ch.click();
        }    
    }
    lastClick.type = 'all';
});

document.getElementById("b2").addEventListener('click', ()=>{
    for(let i=0; i < hide.length; i++) {
        if (hide[i] === false) {
            hide[i] = true;
            let ch = document.querySelector(`input[data-ch-id='${i}']`);
            ch.click();
        }
            
    }
    lastClick.type = 'all';
});

// kliknięcie w nazwę leku, jeśli aktywna
const clickName = (i) => {
    if (lastClick.type === 'name' && lastClick.prod === chartdata[i].producer && lastClick.name === chartdata[i].firstname && lastClick.count % 2 == 1) {
        let tmp = {
            type: 'name',
            prod: lastClick.prod,
            name: lastClick.name,
            count: lastClick.count + 1,
        };
        // odznacz wszystko
        for(let j=0; j < hide.length; j++) {
            if (hide[j] === false && chartdata[j].producer === chartdata[i].producer && chartdata[i].firstname === chartdata[j].firstname) {
                hide[j] = true;
                let ch = document.querySelector(`input[data-ch-id='${j}']`);
                ch.click();
            }
                
        }
        lastClick = tmp;
        
    } else {
        let tmp = {
            type: 'name',
            prod: chartdata[i].producer,
            name: chartdata[i].firstname,
            count: 1
        };
        // zaznacz wszystko
        for(let j=0; j < hide.length; j++) {
            if (hide[j] === true && chartdata[i].producer === chartdata[j].producer && chartdata[i].firstname === chartdata[j].firstname) {
                hide[j] = false;
                let ch = document.querySelector(`input[data-ch-id='${j}']`);
                ch.click();
            }    
        }
        lastClick = tmp;
    }
}

// kliknięcie w nazwę producenta, jeśli aktywna
const clickProd = (i) => {
    if (lastClick.type === 'prod' && lastClick.prod === chartdata[i].producer && lastClick.count % 2 === 1) {
        let tmp = {
            type: 'prod',
            prod: lastClick.prod,
            name: null,
            count: lastClick.count + 1,
        };

        // odznacz wszystko
        for(let j=0; j < hide.length; j++) {
            if (group === 'single' || lock === true) {
                if (hide[j] === false && chartdata[j].producer === chartdata[i].producer) {
                    hide[j] = true;
                    let ch = document.querySelector(`input[data-ch-id='${j}']`);
                    ch.click();
                }
            } else if (group === 'name') {
                if (hide[j] === false && namestats[j].prod === namestats[i].prod) {
                    hide[j] = true;
                    let ch = document.querySelector(`input[data-ch-id='${j}']`);
                    ch.click();
                }
            }
            
                
        }
        lastClick = tmp;
        
    } else {
        let tmp = {
            type: 'prod',
            prod: chartdata[i].producer,
            name: null,
            count: 1
        };
        // zaznacz wszystko
        for(let j=0; j < hide.length; j++) {
            if (group === 'single' || lock === true) {
                if (hide[j] === true && chartdata[i].producer === chartdata[j].producer) {
                    hide[j] = false;
                    let ch = document.querySelector(`input[data-ch-id='${j}']`);
                    ch.click();
                }   
            } else if (group === 'name') {
                if (hide[j] === true && namestats[i].prod === namestats[j].prod) {
                    hide[j] = false;
                    let ch = document.querySelector(`input[data-ch-id='${j}']`);
                    ch.click();
                }   
            }
             
        }
        lastClick = tmp;
    }
    
}


document.getElementById("hidedoughnut").style.display = '';

document.getElementById("2018").setAttribute('disabled', 'disabled');
document.getElementById("2019").setAttribute('disabled', 'disabled');
document.getElementById("cze").setAttribute('disabled', 'disabled');
document.getElementById("gru").setAttribute('disabled', 'disabled');

document.getElementById("2019").addEventListener('click', () => {
    year = 2019;
    document.getElementById("2018").removeAttribute('disabled');
    document.getElementById("2019").setAttribute('disabled', 'disabled');
    createDoughnutChart();
});

document.getElementById("2018").addEventListener('click', () => {
    year = 2018;
    document.getElementById("2019").removeAttribute('disabled');
    document.getElementById("2018").setAttribute('disabled', 'disabled');
    createDoughnutChart();
});

document.getElementById("cze").addEventListener('click', () => {
    sem = 1;

    document.getElementById("gru").removeAttribute('disabled');
    document.getElementById("cze").setAttribute('disabled', 'disabled');
    createDoughnutChart();
});

document.getElementById("gru").addEventListener('click', () => {
    sem = 2;
    document.getElementById("cze").removeAttribute('disabled');
    document.getElementById("gru").setAttribute('disabled', 'disabled');
    createDoughnutChart();
});

createLineChart('nfzpay');

document.getElementById('date').innerHTML = `Wykresy opracowano: ${new Date().toString()}`

document.querySelector("#partbut").addEventListener('click', () => {
    createDoughnutChart(year, sem);

    document.querySelectorAll("#doughnutchoose2 input").forEach(element => {
        element.removeAttribute('disabled');
    });

    if (year === 2019) {
        document.getElementById("2019").setAttribute('disabled', 'disabled');
    } else {
        document.getElementById("2018").setAttribute('disabled', 'disabled');
    }

    if (sem === 1) {
        document.getElementById("cze").setAttribute('disabled', 'disabled');
    } else {
        document.getElementById("gru").setAttribute('disabled', 'disabled');
    }

    enableAllTypes();
    document.querySelector("#groupbut").style.display = '';
    document.querySelector("#partbut").setAttribute('disabled', 'disabled');
    document.getElementById("b1").setAttribute('disabled', 'disabled');
    document.getElementById("b2").setAttribute('disabled', 'disabled');
});

document.querySelector("#singlesgrp").addEventListener('click', () => {
    document.querySelector("#singlesgrp").setAttribute('disabled', 'disabled');
    document.querySelector("#namesgrp").removeAttribute('disabled');
    document.querySelector("#prodsgrp").removeAttribute('disabled');
    hide = null;
    group = 'single';

    if (typ === 'line')
        createLineChart('piecessold');
    else 
        createDoughnutChart(year, sem);
})

document.querySelector("#namesgrp").addEventListener('click', () => {
    document.querySelector("#namesgrp").setAttribute('disabled', 'disabled');
    document.querySelector("#singlesgrp").removeAttribute('disabled');
    document.querySelector("#prodsgrp").removeAttribute('disabled');
    hide = null;
    group = 'name';

    if (typ === 'line')
        createLineChart('piecessold');
    else 
        createDoughnutChart(year, sem);
})

document.querySelector("#prodsgrp").addEventListener('click', () => {
    document.querySelector("#prodsgrp").setAttribute('disabled', 'disabled');
    document.querySelector("#singlesgrp").removeAttribute('disabled');
    document.querySelector("#namesgrp").removeAttribute('disabled');
    hide = null;
    group = 'prod';

    if (typ === 'line')
        createLineChart('piecessold');
    else 
        createDoughnutChart(year, sem);
})

document.getElementById("namesgrp").setAttribute('disabled', 'disabled');
