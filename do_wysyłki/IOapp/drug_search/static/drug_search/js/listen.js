document.querySelector("#piecbut").addEventListener('click', () => {
    createLineChart('piecessold');
    enableAllTypes();
    document.querySelector("#groupbut").style.display = '';
    document.querySelector("#piecbut").setAttribute('disabled', 'disabled');
    document.getElementById("doughnutchoose2").setAttribute('display', 'none');
    document.querySelectorAll("#doughnutchoose2 input").forEach(element => {
        element.setAttribute('disabled', 'disabled');
        document.getElementById("b1").removeAttribute('disabled');
        document.getElementById("b2").removeAttribute('disabled');
    });
});
document.querySelector("#suplbut").addEventListener('click', () => {
    createLineChart('supplement')
    enableAllTypes();
    document.querySelector("#groupbut").style.display = 'none';
    document.querySelector("#suplbut").setAttribute('disabled', 'disabled');
    document.querySelectorAll("#doughnutchoose2 input").forEach(element => {
        element.setAttribute('disabled', 'disabled');
        document.getElementById("b1").removeAttribute('disabled');
        document.getElementById("b2").removeAttribute('disabled');
    });
});
document.querySelector("#nfzbut").addEventListener('click', () => {
    createLineChart('nfzpay');
    enableAllTypes();
    document.querySelector("#groupbut").style.display = 'none';
    document.querySelector("#groupbut").setAttribute('disabled', 'disabled');
    document.querySelector("#nfzbut").setAttribute('disabled', 'disabled');
    document.querySelectorAll("#doughnutchoose2 input").forEach(element => {
        element.setAttribute('disabled', 'disabled');
        document.getElementById("b1").removeAttribute('disabled');
        document.getElementById("b2").removeAttribute('disabled');
    });
});