const basicColorTable = [
    '#000000', '#FF0000', '#00FF00', '#0000FF', '#FFFF00',
    '#00FFFF', '#FF00FF', '#C0C0C0', '#808080', '#800000', '#808000',
    '#008000', '#800080', '#008080', '#000080'
]

const nbColorTable = [
    '#000000', '#00005f', '#000087', '#0000af', '#0000d7', '#0000ff',
    '#005f00', '#005f5f', '#005f87', '#005faf', '#005fd7', '#005fff',
    '#008700', '#00875f', '#008787', '#0087af', '#0087d7', '#0087ff',
    '#00af00', '#00af5f', '#00af87', '#00afaf', '#00afd7', '#00afff',
    '#00d700', '#00d75f', '#00d787', '#00d7af', '#00d7d7', '#00d7ff',
    '#00ff00', '#00ff5f', '#00ff87', '#00ffaf', '#00ffd7', '#00ffff',
    '#5f0000', '#5f005f', '#5f0087', '#5f00af', '#5f00d7', '#5f00ff',
    '#5f5f00', '#5f5f5f', '#5f5f87', '#5f5faf', '#5f5fd7', '#5f5fff',
    '#5f8700', '#5f875f', '#5f8787', '#5f87af', '#5f87d7', '#5f87ff',
    '#5faf00', '#5faf5f', '#5faf87', '#5fafaf', '#5fafd7', '#5fafff',
    '#5fd700', '#5fd75f', '#5fd787', '#5fd7af', '#5fd7d7', '#5fd7ff',
    '#5fff00', '#5fff5f', '#5fff87', '#5fffaf', '#5fffd7', '#5fffff',
    '#870000', '#87005f', '#870087', '#8700af', '#8700d7', '#8700ff',
    '#875f00', '#875f5f', '#875f87', '#875faf', '#875fd7', '#875fff',
    '#878700', '#87875f', '#878787', '#8787af', '#8787d7', '#8787ff',
    '#87af00', '#87af5f', '#87af87', '#87afaf', '#87afd7', '#87afff',
    '#87d700', '#87d75f', '#87d787', '#87d7af', '#87d7d7', '#87d7ff',
    '#87ff00', '#87ff5f', '#87ff87', '#87ffaf', '#87ffd7', '#87ffff',
    '#af0000', '#af005f', '#af0087', '#af00af', '#af00d7', '#af00ff',
    '#af5f00', '#af5f5f', '#af5f87', '#af5faf', '#af5fd7', '#af5fff',
    '#af8700', '#af875f', '#af8787', '#af87af', '#af87d7', '#af87ff',
    '#afaf00', '#afaf5f', '#afaf87', '#afafaf', '#afafd7', '#afafff',
    '#afd700', '#afd75f', '#afd787', '#afd7af', '#afd7d7', '#afd7ff',
    '#afff00', '#afff5f', '#afff87', '#afffaf', '#afffd7', '#afffff',
    '#d70000', '#d7005f', '#d70087', '#d700af', '#d700d7',
]

// osie x-ów

const govlabel = ['sty 2018', 'mar 2018', 'maj 2018', 'lip 2018',
                'wrz 2018', 'lis 2018', 'sty 2019', 'mar 2019',
                'maj 2019', 'lip 2019', 'wrz 2019', 'lis 2019']

const nfzlabel = ['cze 2018', 'gru 2018', 'cze 2019', 'gru 2019'];

// znajduje 7 najlepszych wyników
const find7Best = (array) => {
    let best = array.slice();
    best.sort((a,b) => {
        return b.val - a.val;
    })
    let best2 = best.slice(0,7);
    best2.sort((a,b) => {
        return a.id - b.id;
    });
    return best2;
}

// From https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array#6274398
const shuffle = (array) => {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

// resetuje canvas
const cleanCanvasContainer = () => {
    document.getElementById("w1").innerHTML = '&nbsp;';
    document.getElementById("w1").innerHTML = '<canvas id="chart"></canvas>';
}


const spis = document.getElementById('spis'); // odwołanie do tabeli legendy

const enableAllTypes = () => {
    document.querySelector("#piecbut").removeAttribute('disabled');
    document.querySelector("#suplbut").removeAttribute('disabled');
    document.querySelector("#nfzbut").removeAttribute('disabled');
    document.querySelector("#partbut").removeAttribute('disabled');
}

const min =  (a,b) => {
    if (a > b) return b;
    else return a;
};

const alphaorder = (a,b) => {
    if (a.producer < b.producer) 
        return -1;
    if (a.producer > b.producer) 
        return 1;
    if (a.name < b.name) 
        return -1;
    if (a.name > b.name) 
        return 1;
    if (`${a.form}, ${a.dose}, ${a.package}` < `${b.form}, ${b.dose}, ${b.package}`) 
        return -1;
    if (`${a.form}, ${a.dose}, ${a.package}` >= `${b.form}, ${b.dose}, ${b.package}`) 
        return 1;
}

const alphaorder2 = (a,b) => {
    if (a.prod < b.prod) 
        return -1;
    if (a.prod > b.prod) 
        return 1;
    if (a.name < b.name) 
        return -1;
    return 1;
}

const alphaorder3 = (a,b) => {
    if (a.prod < b.prod) 
        return -1;
    return 1;
}

const valorder = (a,b) => {
    return b.val - a.val;
}
