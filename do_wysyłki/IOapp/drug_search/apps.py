from django.apps import AppConfig


class DrugSearchConfig(AppConfig):
    name = 'drug_search'
